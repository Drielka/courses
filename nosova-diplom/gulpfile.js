const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const imagemin = require('gulp-imagemin');
const watch = require('gulp-watch');


let sassTask = function (){
  return gulp.src('./scss/**/*.scss')
    .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./css/'))
}

let imageminTask = function () {
  gulp.src('./img/*')
      .pipe(imagemin())
      .pipe(gulp.dest('./img'));
}


let fontsTask = function () {
    return gulp.src('./fonts/*')
        .pipe(gulp.dest('./fonts'))
}

let watcher = function () {
  gulp.watch('./scss/**/*.scss', ['sass']);
  gulp.watch('./img/*', ['image']);
  gulp.watch('./fonts/*', ['fonts']);
}

gulp.task('imagemin', imageminTask);

gulp.task('sass', sassTask);

gulp.task('fonts', fontsTask);

gulp.task('watch', watcher);
/*
gulp.task('default', ['watch', 'fonts', 'imagemin']);
*/
